import { notification } from 'antd/lib/index';

const textError = 'Error network connection';

const openNotificationWithIcon = (
  title = textError,
  status
) => {
  notification['error']({
    message: status > 0 ? status : '',
    description: title,
  });
};

export default (error) => {
  if (error.response) {
    openNotificationWithIcon(error.response.data.error_message, error.response.status);
  } else if (error.request) {
    openNotificationWithIcon(textError, error.request.status);
  } else {
    openNotificationWithIcon(error);
  }

  console.error('error.config-', error.config);
};
