import axios from 'axios';
import handleError from 'utils/handleError';

const http = axios.create ({
  headers: {'Content-Type': 'application/json'},
  timeout: 2000,
});


export function api(type) {
  let url;
  switch (type) {
    case 'nav':
      url = 'https://api.memory.com/nav/v1';
      break;
    case 'search':
      url = 'https://api.memory.com/search/v1';
      break;
    case 'answer':
      url = 'https://api.memory.com/answers/v1';
      break;
    case 'course':
      url = 'https://t6.memory.com/v1';
      break;
    default:
      url = ''
  }

  http.interceptors.response.use(response => response, (error) => {
    const { config } = error;
    handleError(error);
    if (error && config && !config.__isRetryRequest) {
      config.__isRetryRequest = true;
      return axios(config);
    }
    throw error;
  });

  http.defaults.baseURL = url;

  return http
}