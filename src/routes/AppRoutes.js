import React from 'react';

import {
  DashboardPage,
  ProfilePage,
  LeaderBoardPage,
  CoursesPage,
  LearnPage
} from 'pages';

const AppRoutes = [
  {
    path: '/dashboard',
    component: (props = {}) => <DashboardPage {...props} />,
    label: 'Dashboard'
  },
  {
    path: '/profile/:profileId',
    component: (props = {}) => <ProfilePage {...props} />,
    label: 'Profile'
  },
  {
    path: '/profile',
    component: (props = {}) => <ProfilePage {...props} />,
    label: 'Profile'
  },
  {
    path: '/leaderboard',
    component: (props = {}) => <LeaderBoardPage {...props} />,
    label: 'LeaderBoard'
  },
  {
    path: '/courses',
    component: (props = {}) => <CoursesPage {...props} />,
    label: 'Courses'
  },
  {
    path: '/learn/:mode/:categoryId',
    component: (props = {}) => <LearnPage {...props} />,
    label: 'Learn'
  },
];

export default AppRoutes;