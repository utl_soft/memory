import { api as apiClient } from 'utils/api';
import __CONFIG__ from 'config';

const api = {
  getCourses: async () => await apiClient('course')(`/categories`).then(response => {
    return response;
  }),
  getCurrentCourse: async (categoryId) => await apiClient('nav')(`/categories/${categoryId}?user_id=${__CONFIG__.userId}`).then(response => {
    return response;
  }),
};

export default api;
