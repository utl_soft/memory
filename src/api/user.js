import { api as apiClient } from 'utils/api';
import __CONFIG__ from 'config';

const api = {
  getUserInfo: async () => await apiClient('answer')(`/user/profile?user_id=${__CONFIG__.userId}`).then(response => {
    return response;
  }),
  getUserStats: async () => await apiClient('nav')(`/nav-bar?user_id=${__CONFIG__.userId}`).then(response => {
    return response;
  }),
};

export default api;
