import { api as apiClient } from 'utils/api';

const api = {
  sendSearch: async (value) => await apiClient('search')(`/global?q=${value}`).then(response => {
    return response;
  }),
};

export default api;
