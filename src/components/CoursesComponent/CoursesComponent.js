import React, { Component } from 'react';

import { Card, Icon, Select } from 'antd';

import styled from 'styled-components';

import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';

const { Meta } = Card;
const Option = Select.Option;

const ModeContainer = styled('div')`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const CardCourse = styled(Card)`
  margin-bottom: 25px;
`;

const RowCourses = styled('div')`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding-right: 25px;
`;

class CoursesComponent extends Component {
  state = {
    courses: []
  };

  componentDidMount() {
    this.props.courses.map(x => x.mode = 'smart');
    this.setState({ courses: this.props.courses });
  }

  updateState = index => {
    return {
      ...this.state,
      data: this.state.courses.map((item, idx) => idx === index ? { ...this.state.courses[index] } : {...item})
    };
  };


  handleChange = (value, course, index) => {
    const {data} = this.updateState(index);
    data[index].mode = value;
    this.setState({ courses: data });
  };

  handlePlay = course => {
    this.props.history.replace(`/learn/${course.mode}/${course.unique_id}`);
  };

  render() {
    const { courses } = this.state;

    return (
     <RowCourses>
       {
         courses && courses.map((course, index) => (
           <CardCourse
             key={course.unique_id}
             style={{ width: 300 }}
             actions={[
               <label>English to English</label>,
               <Icon type="play-circle" theme="outlined" onClick={() => this.handlePlay(course)} />
             ]}
           >
             <Meta
               title={course.label}
               description={[
                 <ModeContainer key={1}>
                   <label>Select Mode</label>
                   <Select
                     defaultValue="smart"
                     style={{ width: 160 }}
                     onChange={(e) => this.handleChange(e, course, index)}
                   >
                     <Option value="smart">Smart</Option>
                     <Option value="typing">Typing</Option>
                     <Option value="multiple">Multiple</Option>
                     <Option value="describe-image">Image</Option>
                     <Option value="describe-image-nouns">Image Multi</Option>
                     <Option value="listening">Listening</Option>
                   </Select>
                 </ModeContainer>
               ]}
             />
           </CardCourse>
         ))
       }
     </RowCourses>
    )
  }
}

export default withRouter(connect()(CoursesComponent));