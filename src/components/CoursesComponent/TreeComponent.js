import React, { Component } from 'react';
import {connect} from 'react-redux';

import styled from 'styled-components';

const ContainerCategory = styled('div')`
  opacity: 0.8;
  border-radius: 50%;
  width: 105px;
  height: 105px;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: center;
  margin-bottom: 25px;
  flex-direction: column;
  margin-right: 25px;
  border: ${props => !props.blocked ? '1px solid #e7e7e7' : '1px solid #1890ff'};
  background: ${props => !props.blocked ? '#e7e7e7' : 'transparent'}
  &:hover {
    cursor: pointer;
  }
  div {
    z-index: 999;
    text-align: center;
  }
  &:after {
    content: '';
    width: 105px;
    height: 105px;
    background-image: ${props => `linear-gradient(21deg, white 0%, transparent 0), linear-gradient(0deg, #1890ff ${props.percentage}%, transparent 0)`};
    position: absolute;
    border-radius: 50%;
  }
`;


const Row = styled('div')`
  max-width: 400px;
  width: 100%;
  display: flex;
  justify-content: space-evenly;
`;

const Col = styled('div')`
  display: flex;
`;

const Card = styled('div')`
  padding: 25px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border-bottom: 1px dashed #e7e7e7;
`;

const mapStateToProps = state => ({
  courses: state.courses.currentCourse
});

class TreeComponent extends Component {
  handleCategory = category => {
    if (!category.is_unlocked) {
      return false;
    }
  };

  render() {
    const { categories } = this.props;
    let roots = [];

    categories.category.forEach(node => {
      let oneFrom = categories.ids.find(x => x.to === node.category_id);
      if (!oneFrom) {
        roots = [
          ...roots,
          node
        ];
        return roots;
      }
    });

    let trees = [];

    roots.forEach((root, index) => {
      let rootArray = [];
      let tree = [];

      rootArray = [
        ...rootArray,
        root
      ];
      let i = 0;

      while (rootArray.length !== 0) {
        let col = [];

        let rootLength = rootArray.length;

        while (rootLength > 0) {
          let category = rootArray.shift();
          const uniqIndex = Math.random();

          col = [
            ...col,
            <ContainerCategory
              key={uniqIndex}
              percentage={category.progress.percentage_completed}
              blocked={category.is_unlocked}
              onClick={() => this.handleCategory(category)}
            >
              <div>
                {category.progress.percentage_completed}<span>%</span>
              </div>
              <div>
                {category.name}
              </div>
            </ContainerCategory>
          ];

          let columns = categories.ids.filter(x => x.from === category.category_id);
          columns.forEach(column => {
            let col = categories.category.find(x => x.category_id === column.to);

            rootArray = [
              ...rootArray,
              col
            ];
          });

          rootLength--;
        }

        tree.push(
          <Row key={i}>
            <Col>
              {col}
            </Col>
          </Row>
        );
        i++;
      }

      trees.push(
        <Card key={index}>
          {tree}
        </Card>
      );
    });
    return trees;
  }
}

export default connect(mapStateToProps, null)(TreeComponent)