import React from 'react';

import { connect } from 'react-redux';

import styled from 'styled-components';

const ContainerStats = styled('div')`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

const Stat = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  line-height: 1.65;
  color: #fff;
  font-weight: 500;
  font-size: 13px;
  div {
    color: #e7e7e7;
  }
  span {
    font-size: 15px;
    font-weight: 700;
  }
  i {
    margin-right: 0px !important;
  }
`;

const UserStats = ({ userStats }) => {
  return (
    <ContainerStats>
      <Stat>
        <span>XP</span>
        <div>{userStats.total_xp}</div>
      </Stat>
      <Stat>
        <span>Time</span>
        <div>{userStats.number_questions_due}</div>
      </Stat>
      <Stat>
        <span>Mail</span>
        <div>{userStats.unread_messages}</div>
      </Stat>
      <Stat>
        <span>Streak</span>
        <div>{userStats.consecutive_days_played}</div>
      </Stat>
      <Stat>
        <span>Level</span>
        <div>{userStats.user_level}</div>
      </Stat>
      <Stat>
        <span>Recent Course</span>
        <div>{userStats.current_course}</div>
      </Stat>
    </ContainerStats>
  )
};

export default connect()(UserStats)