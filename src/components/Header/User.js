import React from 'react';

import { List, Avatar } from 'antd';

import styled from 'styled-components';

const UserInfo = styled(List.Item)`
  padding: 0;
  span {
    color: #fff !important;
  }
  .ant-list-item-meta-description {
    color: #fff !important;
  }
  .ant-list-item-meta {
    align-items: center;
  }
`;

export const User = ({ user }) => {
  return (
    <UserInfo>
      <List.Item.Meta
        avatar={<Avatar src={user.user.picture_url !== '' ? user.user.picture_url : 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'} />}
        title={<span>{user.user.name !== '' ? user.user.name : 'Sanqa Gusev'}</span>}
        description={user.user.email !== '' ? user.user.email : 'mail@mail.com'}
      />
    </UserInfo>
  )
}