import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import {Link} from 'react-router-dom';

import {connect} from 'react-redux';

const Container = styled('div')`

`;

const Row = styled('div')`
  display: flex;
  justify-content: flex-start;
  border-bottom: 1px dashed #e7e7e7;
  flex-wrap: wrap;
`;

const Item = styled('div')`
  max-height: 35px;
  display: flex;
  align-items: center;
  flex: 0 1 30%;
`;

const mapStateToProps = state => ({
  results: state.search.searchResult
});

const SearchResult = ({results}) => {
  function sortingResult(type, data) {
    switch (type) {
      case 'categories':
        return (
          <Row>
            <Item>
              <span className="bold">Type: </span>
              <span>{data.type}</span>
            </Item>
            <Item>
              <span className="bold">Label: </span>
              <span>{data.label}</span>
            </Item>
            <Item className="card-slug">
              <span className="bold">Slug: </span>
              <span>{data.slug}</span>
            </Item>
          </Row>
        );
      case 'users':
        return (
          <Row>
            <Item>
              <span className="bold">Type: </span>{data.type}
            </Item>
            <Item>
              <span className="bold">Name: </span><Link id="profile-link" to={`/profile/${data.id}`}>{data.name}</Link>
            </Item>
            <Item>
              <span className="bold">Email: </span>{data.email}
            </Item>
            <Item>
              <span className="bold">Reputation: </span>{data.reputation}
            </Item>
          </Row>
        );
      default:
        return null
    }
  }

  return (
    <div>
      {
        results && results.length > 0 ?
          <div>
            {results.map((result, index) => {
              return (
                <Container key={index}>
                  {sortingResult(result.type, result)}
                </Container>
              )
            })}
          </div> : <div style={{textAlign: 'center'}}>Not Found</div>
      }
    </div>
  )
};

SearchResult.propTypes = {
  products: PropTypes.array
};

export default connect(mapStateToProps, null)(SearchResult);