import React, { Component } from 'react';

import styled from 'styled-components';

import { Input } from 'antd';

import {connect} from 'react-redux';

import { sendSearch } from 'actions/search';

import SearchResult from './SearchResult';

import store from 'store';

const Search = Input.Search;

const SearchContainer = styled('div')`
  width: 55%;
  display: flex;
  justify-content: flex-end;
  padding-right: 25px;
`;

const mapStateToProps = state => ({
  results: state.search.searchResult
});

class SearchBar extends Component {
  state = {
    isVisibleSearchResult: false
  };

  componentDidMount() {
    document.body.addEventListener('click', this.openSearchList);
  }

  componentWillUnmount() {
    document.body.removeEventListener('click', this.openSearchList);
  }

  openSearchList = e => {
    const { results } = this.props;
    const elementSearchList = e.path.find(x => x.id === 'search-result');
    const elementSearchForm = e.path.find(x => x.id === 'search-form');
    const elementLinkToProfile = e.path.find(x => x.id === 'profile-link');

    if (typeof elementLinkToProfile !== 'undefined') {
      this.setState({
        isVisibleSearchResult: false
      })
    }

    if (typeof elementSearchList === 'undefined') {
      this.setState({
        isVisibleSearchResult: false
      })
    }

    if (results.length > 0 && elementSearchForm) {
      this.setState({
        isVisibleSearchResult: true
      })
    }
  };

  handleChangeSearchProduct = event => {
    store.dispatch(sendSearch(event.target.value));
    this.setState({
      isVisibleSearchResult: true
    });
  };

  render() {
    return (
      <SearchContainer>
        <Search
          id="search-form"
          placeholder="Search"
          onSearch={value => console.log(value)}
          onChange={(e) => this.handleChangeSearchProduct(e)}
          style={{ width: 500 }}
        />
        <div id="search-result" className={this.state.isVisibleSearchResult ? 'visible' : 'hidden'}>
          <SearchResult />
        </div>
      </SearchContainer>
    )
  }
}

export default connect(mapStateToProps, null)(SearchBar);