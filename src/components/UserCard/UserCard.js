import React from 'react';

import styled from 'styled-components';

import {List, Card, Spin} from 'antd';

const Row = styled('div')`
  display: flex;
  flex-direction: column;
`;

const CardContainer = styled(List.Item)`
  max-width: 400px;
  text-align: center;
  .ant-card-head-title {
    justify-content: center;
  }
  .ant-card-head {
    background: #f3f3f3;
  }
`;

const ListCard = styled('div')`

`;

export const UserCard = ({ user }) => {

  return (
    <Row>
      {user.load ? <Spin size="large" /> :
       <Row>
         {
           user &&
           <ListCard>
             <CardContainer key={1}>
               <Card title="Level">{user.user.level}</Card>
             </CardContainer>
             <CardContainer key={2}>
               <Card title="XP">{user.user.total_xp}</Card>
             </CardContainer>
             <CardContainer key={3}>
               <Card title="Reputation">{user.user.reputation}</Card>
             </CardContainer>
             <CardContainer key={4}>
               <Card title="Last played">{user.user.last_played_date !== '' ? new Date(user.user.last_played_date).toLocaleDateString() : 0}</Card>
             </CardContainer>
             <CardContainer key={5}>
               <Card title="Time Zone">{user.user.time_zone !== '' ? user.user.time_zone : '-'}</Card>
             </CardContainer>
             <CardContainer key={6}>
               <Card title="Registered">a day ago</Card>
             </CardContainer>
             <CardContainer key={7}>
               <Card title="Total time online">{user.user.total_time_online !== '' ? user.user.total_time_online : '0 hour'}</Card>
             </CardContainer>
           </ListCard>
         }
       </Row>
      }
    </Row>
  )
};