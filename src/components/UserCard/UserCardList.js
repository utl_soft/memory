import React from 'react';

import { List } from 'antd';

import { UserCard } from './UserCard';

import { connect } from 'react-redux';
import { withRouter} from "react-router-dom";

import styled from 'styled-components';

const ListUserCard = styled(List)`

`;

const mapStateToProps = state => ({
  user: state.user
});

const UserCardList = ({ user }) => {
  return (
    <ListUserCard
      grid={{ gutter: 16, xs: 1, sm: 2, md: 4, lg: 4, xl: 6, xxl: 3 }}
    >
      <UserCard user={user} />
    </ListUserCard>
  )
};

export default withRouter(connect(mapStateToProps, null)(UserCardList));