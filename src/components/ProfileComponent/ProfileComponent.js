import React, { Component } from 'react';

import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import styled from 'styled-components';

const ListProfile = styled('div')`
  display: flex;
  justify-content: space-between;
  max-width: 1200px;
`;

const ItemProfile = styled('div')`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px dashed #e7e7e7;
  padding: 10px 0 10px 0;
  margin-bottom: 15px;
`;

const ItemProfileLabel = styled('div')`
  font-size: 18px;
  font-weight: 600;
`;

const ItemProfileContent = styled('div')`

`;

const Container = styled('div')`
  flex: 0 1 45%;
`;

class ProfileComponent extends Component {
  render() {
    const { user } = this.props;
    return (
      <ListProfile>
        <Container>
          <ItemProfile>
            <ItemProfileLabel>Name</ItemProfileLabel>
            <ItemProfileContent>{user.name} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Email</ItemProfileLabel>
            <ItemProfileContent>{user.email} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Time Zone</ItemProfileLabel>
            <ItemProfileContent>{user.time_zone} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Total Answers</ItemProfileLabel>
            <ItemProfileContent>{user.total_answers} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Total Correct Answers</ItemProfileLabel>
            <ItemProfileContent>{user.total_correct_answers} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Total Wrong Answers</ItemProfileLabel>
            <ItemProfileContent>{user.total_wrong_answers} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Total Xp</ItemProfileLabel>
            <ItemProfileContent>{user.total_xp} </ItemProfileContent>
          </ItemProfile>
        </Container>
        <Container>
          <ItemProfile>
            <ItemProfileLabel>Avg Correct Percent</ItemProfileLabel>
            <ItemProfileContent>{user.avg_correct_percent} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Avg Hestitation Time</ItemProfileLabel>
            <ItemProfileContent>{user.avg_hestitation_time} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Avg Total Time</ItemProfileLabel>
            <ItemProfileContent>{user.avg_total_time} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Avg Wpm</ItemProfileLabel>
            <ItemProfileContent>{user.avg_wpm} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Consecutive Days</ItemProfileLabel>
            <ItemProfileContent>{user.consecutive_days} </ItemProfileContent>
          </ItemProfile>
          <ItemProfile>
            <ItemProfileLabel>Session Questions Count</ItemProfileLabel>
            <ItemProfileContent>{user.session_questions_count} </ItemProfileContent>
          </ItemProfile>
        </Container>
      </ListProfile>
    )
  }
}

export default withRouter(connect()(ProfileComponent))