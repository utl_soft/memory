import React, {Component} from 'react';

import {
  ComposedChart,
  Bar,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis
} from 'recharts';

import styled from 'styled-components';

const ContainerTitleGraph = styled('div')`
  font-size: 36px;
  text-align: center;
  margin: 25px 0 25px 0;
  font-weight: 500;
  border-bottom: 1px dashed #e7e7e7;
`;

export default class ProfileGraph extends Component {

  render() {
    const { graph, xKey, yKey } = this.props;
    return (
      <div>
        <ContainerTitleGraph>Experience earned by day</ContainerTitleGraph>
        {
          graph &&
          <ResponsiveContainer width="100%" height={300} aria-labelledby="tableTitle">
            <ComposedChart
              data={Object.values(graph)}
            >
              <XAxis dataKey={xKey}/>
              <YAxis />
              <Tooltip/>
              <Bar dataKey={yKey} fill="#413ea0"/>
            </ComposedChart>
          </ResponsiveContainer>
        }
      </div>
    )
  }
}