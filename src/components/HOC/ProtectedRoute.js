import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';


export default (Component) => {

  class ProtectedRoute extends React.Component {
    static contextTypes = {
      router: PropTypes.object.isRequired,
    };

    render() {
      return <Component {...this.props} />;
    }
  }

  return withRouter(connect()(ProtectedRoute));
};
