import React, { Component } from 'react';

import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { api } from 'utils/api';

import { columns } from 'components/LeaderBoardComponent/tableColumns';

import { Table, Spin } from 'antd';
import styled from 'styled-components';

const StyledTable = styled(Table)`
  width: 100%;

  & td, & td * {
    cursor: default !important;
  }
  th {
    background: transparent !important;
  }
`;

class LeaderBoardComponent extends Component {
  state = {
    users: null
  };

  async componentDidMount() {
    await api('answer')('/leaderboard/').then(response => {
      const users = Object.values(response.data.user_details);
      users.map(user => {
        const d = new Date(user.last_played_date);
        const date = d.getDate();
        const month = d.getMonth() + 1;
        const year = d.getFullYear();
        return user.last_played_date = user.last_played_date !== '0000-00-00' ? month + '.' + date + '.' + year : '';
      });
      this.setState({ users });
    })
  }

  render() {
    const { users } = this.state;

    return (
      <div style={{ textAlign: 'center' }}>
        {
          users ?
            <StyledTable
              columns={columns}
              scroll={{ y: 700 }}
              dataSource={users}
              pagination={false}
              rowKey="id"
            /> : <Spin size="large" />
        }
      </div>
    )
  }
}

export default withRouter(connect()(LeaderBoardComponent))