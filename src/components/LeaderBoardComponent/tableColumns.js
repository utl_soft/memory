import React from 'react';

export const columns = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name',
  width: '200px',
}, {
  title: 'Total XP',
  dataIndex: 'total_xp',
  key: 'total_xp',
  width: '200px',
}, {
  title: 'Reputation',
  dataIndex: 'reputation',
  key: 'reputation',
  width: '200px',
}, {
  title: 'Avg WPM',
  dataIndex: 'avg_wpm',
  key: 'avg_wpm',
  width: '200px',
},
{
  title: 'Avg Correct Percent',
  dataIndex: 'avg_correct_percent',
  key: 'avg_correct_percent',
  width: '200px',
},
{
  title: 'Last Played',
  dataIndex: 'last_played_date',
  key: 'last_played_date',
  width: '200px',
},
{
  title: 'Total Answers',
  dataIndex: 'total_answers',
  key: 'total_answers',
  width: '200px',
},
{
  title: 'Correct Answers',
  dataIndex: 'total_correct_answers',
  key: 'total_correct_answers',
  width: '200px',
},
{
  title: 'Consecutive Days',
  dataIndex: 'consecutive_days',
  key: 'consecutive_days',
  width: '200px',
}];