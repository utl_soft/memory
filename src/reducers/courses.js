import AppConstants from 'constants/AppConstants';

const initialState = {
  courses: null,
  currentCourse: null,
  load: false
};

export default function courses(state = initialState, { type, payload, load }) {
  switch (type) {
    case AppConstants.GET_COURSES_START:
    case AppConstants.GET_CURRENT_COURSE_START:
      return {
        ...state,
        load
      };
    case AppConstants.GET_COURSES_SUCCESS:
    case AppConstants.GET_COURSES_FAILURE:
      return {
        ...state,
        courses: payload,
        load
      };
    case AppConstants.GET_CURRENT_COURSE_SUCCESS:
    case AppConstants.GET_CURRENT_COURSE_FAILURE:
      return {
        ...state,
        currentCourse: payload,
        load
      };
    default:
      return state;
  }
}