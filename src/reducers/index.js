import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import user from 'reducers/user'
import search from 'reducers/search'
import courses from 'reducers/courses'

export default combineReducers({
  routing: routerReducer,
  user,
  search,
  courses
});