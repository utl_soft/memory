import AppConstants from 'constants/AppConstants';

const initialState = {
  user: {},
  userStats: {},
  load: false
};

export default function user(state = initialState, { type, payload, load }) {
  switch (type) {
    case AppConstants.GET_USER_INFO_START:
    case AppConstants.GET_USER_STATS_START:
      return {
        ...state,
        load
      };
    case AppConstants.GET_USER_STATS_SUCCESS:
    case AppConstants.GET_USER_STATS_FAILURE:
      return {
        ...state,
        userStats: payload,
        load
      };
    case AppConstants.GET_USER_INFO_SUCCESS:
    case AppConstants.GET_USER_INFO_FAILURE:
      return {
        ...state,
        user: payload,
        load
      };
    default:
      return state;
  }
}