import AppConstants from 'constants/AppConstants';

const initialState = {
  searchResult: {},
  load: false
};

export default function search(state = initialState, { type, payload, load }) {
  switch (type) {
    case AppConstants.SEND_SEARCH_START:
      return {
        ...state,
        load
      };
    case AppConstants.SEND_SEARCH_SUCCESS:
    case AppConstants.SEND_SEARCH_FAILURE:
      return {
        ...state,
        searchResult: payload,
        load
      };
    case AppConstants.GET_USER_INFO_SUCCESS:
    default:
      return state;
  }
}