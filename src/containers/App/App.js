import React, { Component } from 'react';

import { Layout, Menu, Icon } from 'antd';

import styled from "styled-components";

import { withRouter, Link } from 'react-router-dom';

import { connect } from 'react-redux';

import Page from './Page';

import SearchBar from 'components/Header/SearchBar';

import { User } from 'components/Header/User';
import UserStats from 'components/Header/UserStats';

const { Content, Header } = Layout;

const Row = styled(Layout)`
  .ant-layout {
    background: #fff !important;
    flex-direction: row !important;
  }
  background: #fff !important;
  flex-direction: row !important;
  .ant-menu {
    max-width: 211px;
  }
  li.ant-menu-item {
    display: flex;
    align-items: center;
    &:hover {
      cursor: default !important;
    }
  }
`;

const ContentRoute = styled(Content)`
  padding-left: 25px;
`;

const MenuBar = styled(Menu)`
  display: flex;
  align-items: center;
  height: 100%;
  li.ant-menu-item {
    background: transparent !important;
    height: 100%;
    display: flex;
    align-items: center;
    flex: 0 1 50%;
    &:hover {
      cursor: default !important;
    }
  }
`;

const MenuHeader = styled(Header)`
  padding: 0px 7px;
`;

const mapStateToProps = state => ({
  user: state.user,
  userStats: state.user.userStats
});

class App extends Component {
  render() {
    const { location, user, userStats } = this.props;
    return (
      <Layout>
        <MenuHeader className="header">
          <div className="logo" />
          <MenuBar
            theme="dark"
            style={{ lineHeight: '64px' }}
          >
            <Menu.Item key="1">
              <User user={user} />
            </Menu.Item>
            <Menu.Item key="2">
              <UserStats userStats={userStats} />
            </Menu.Item>
            <SearchBar />
          </MenuBar>
        </MenuHeader>
        <Row>
          <Menu
            mode="inline"
            defaultSelectedKeys={['/dashboard']}
            selectedKeys={[location.pathname]}
          >
            <Menu.Item key="/dashboard">
              <Link to="/dashboard">
                <Icon type="dashboard" theme="outlined" />
                Dashboard
              </Link>
            </Menu.Item>
            <Menu.Item key="/leaderboard">
              <Link to="/leaderboard">
                <Icon type="star" theme="outlined" />
                Leaderboard
              </Link>
            </Menu.Item>
            <Menu.Item key="/courses">
              <Link to="/courses">
                <Icon type="book" theme="outlined" />
                Courses
              </Link>
            </Menu.Item>
            <Menu.Item key="/profile">
              <Link to="/profile">
                <Icon type="user" theme="outlined" />
                User Profile
              </Link>
            </Menu.Item>
          </Menu>
          <Layout>
            <ContentRoute>
              <Page />
            </ContentRoute>
          </Layout>
        </Row>
      </Layout>
    );
  }
}

export default withRouter(connect(mapStateToProps, null)(App));
