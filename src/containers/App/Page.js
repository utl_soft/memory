import React from 'react';
import {connect} from 'react-redux';
import {withRouter, Route, Switch} from 'react-router-dom';
import AppRoutes from 'routes/AppRoutes';

import styled from 'styled-components';

const Layouts = styled.div`
  padding-top: 25px;
`;

class Page extends React.Component {

  constructor(props) {
    super(props);

    this.state = {};
  }

  getRoutes = (routesArray, key = 0) => (

    routesArray.map((route, index) => {
      if (route.routes === undefined) {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        );
      } else {
        return (
          <Switch key={index}>
            {this.getRoutes(route.routes, key + 1)}
          </Switch>
        );
      }
    })
  );

  render() {
    return (
      <Layouts>
        <Switch>
          <Route path='/' exact={true} component={AppRoutes[0].component}/>
          {this.getRoutes(AppRoutes)}
        </Switch>
      </Layouts>
    );
  }
}


export default withRouter(connect()(Page));