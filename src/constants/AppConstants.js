import keyMirror from 'keymirror';

export default keyMirror({
  GET_USER_INFO_START: null,
  GET_USER_INFO_SUCCESS: null,
  GET_USER_INFO_FAILURE: null,

  // User Stats
  GET_USER_STATS_START: null,
  GET_USER_STATS_SUCCESS: null,
  GET_USER_STATS_FAILURE: null,

  // Search
  SEND_SEARCH_START: null,
  SEND_SEARCH_SUCCESS: null,
  SEND_SEARCH_FAILURE: null,

  // Courses
  GET_COURSES_START: null,
  GET_COURSES_SUCCESS: null,
  GET_COURSES_FAILURE: null,

  GET_CURRENT_COURSE_START: null,
  GET_CURRENT_COURSE_SUCCESS: null,
  GET_CURRENT_COURSE_FAILURE: null
});