import React from 'react';
import ReactDOM from 'react-dom';

import * as serviceWorker from './serviceWorker';

import { Provider } from 'react-redux';
import store from 'store';

import './styles/index.scss';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import App from 'containers/App/App';

import ProtectedRoute from 'components/HOC/ProtectedRoute';

import { getUserInfo } from 'actions/user';
import { getCourses } from 'actions/courses';

store.dispatch(getUserInfo());
store.dispatch(getCourses());

function renderApp () {
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/" component={ProtectedRoute(App)}/>
        </Switch>
      </Router>
    </Provider>,
    document.getElementById("root")
  );
}

serviceWorker.unregister();

renderApp();
