import api from 'api/courses';

import AppConstants from 'constants/AppConstants';

export const getCourses = () => async dispatch => {
  dispatch({
    type: AppConstants.GET_COURSES_START,
    load: true
  });

  try {
    const { data: { data: { categories } } } = await api.getCourses();

    dispatch({
      type: AppConstants.GET_COURSES_SUCCESS,
      payload: categories,
      load: false
    });
  } catch (e) {
    dispatch({
      type: AppConstants.GET_COURSES_FAILURE,
      payload: false,
      error: true
    });
  }
};

export const getCurrentCourse = (categoryId) => async dispatch => {
  dispatch({
    type: AppConstants.GET_CURRENT_COURSE_START,
    load: true
  });

  try {
    const { data } = await api.getCurrentCourse(categoryId);

    dispatch({
      type: AppConstants.GET_CURRENT_COURSE_SUCCESS,
      payload: data,
      load: false
    });
  } catch (e) {
    dispatch({
      type: AppConstants.GET_CURRENT_COURSE_FAILURE,
      payload: false,
      error: true
    });
  }
};

