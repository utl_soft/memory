import api from 'api/user';

import AppConstants from 'constants/AppConstants';

import store from 'store';

export const getUserInfo = () => async dispatch => {
  dispatch({
    type: AppConstants.GET_USER_INFO_START,
    load: true
  });

  try {
    const { data: { data: { user } } } = await api.getUserInfo();

    dispatch({
      type: AppConstants.GET_USER_INFO_SUCCESS,
      payload: user,
      load: false
    });
    store.dispatch(getUserStats());
  } catch (e) {
    dispatch({
      type: AppConstants.GET_USER_INFO_FAILURE,
      payload: false,
      error: true
    });
  }
};

export const getUserStats = () => async dispatch => {
  dispatch({
    type: AppConstants.GET_USER_STATS_START,
    load: true
  });

  try {
    const { data } = await api.getUserStats();

    dispatch({
      type: AppConstants.GET_USER_STATS_SUCCESS,
      payload: data,
      load: false
    });
  } catch (e) {
    dispatch({
      type: AppConstants.GET_USER_STATS_FAILURE,
      payload: false,
      error: true
    });
  }
};

