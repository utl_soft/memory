import api from 'api/search';

import AppConstants from 'constants/AppConstants';

export const sendSearch = value => async dispatch => {
  dispatch({
    type: AppConstants.SEND_SEARCH_START,
    load: true
  });

  try {
    const { data } = await api.sendSearch(value);

    dispatch({
      type: AppConstants.SEND_SEARCH_SUCCESS,
      payload: data,
      load: false
    });
  } catch (e) {
    dispatch({
      type: AppConstants.SEND_SEARCH_FAILURE,
      payload: false,
      error: true
    });
  }
};

