import React, { Component } from 'react';

import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';

import UserCardList from 'components/UserCard/UserCardList';

class DashboardPage extends Component {
  render() {
    return (
      <div>
        <UserCardList />
      </div>
    )
  }
}

export default withRouter(connect()(DashboardPage));