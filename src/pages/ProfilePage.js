import React, { Component } from 'react';

import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';

import ProfileComponent from 'components/ProfileComponent/ProfileComponent';
import ProfileGraph from 'components/ProfileComponent/ProfileGraph';

import { api } from 'utils/api';

const mapStateToProps = state => ({
  user: state.user
});

class ProfilePage extends Component {
  state = {
    user: null,
    userId: this.props.match.params.profileId ? this.props.match.params.profileId : null
  };

  componentDidMount() {
    if (this.props.match.params && this.props.match.params.profileId) {
      this.searchUser(this.props.match.params.profileId);
    }
  }

  searchUser = async userId => {
    const { data: { data: { user } } } = await api('answers')(`/user/profile?user_id=${userId}`);
    this.setState({
      user,
      userId
    });
  };

  render() {
    let { user: { user } } = this.props;
    if (this.props.match.params && this.props.match.params.profileId) {
      if (this.props.match.params.profileId !== this.state.userId) {
        this.searchUser(this.props.match.params.profileId);
      }
    }

    if (this.state.user) {
      user = this.state.user;
    }

    return (
      <div>
        <h1>User Profile</h1>
        {
          !user.load &&
            <div>
              <ProfileComponent user={user} />
              <ProfileGraph
                graph={user.graph_xp_by_day}
                xKey="X"
                yKey="Y"
              />
            </div>
        }
      </div>
    )
  }
}

export default withRouter(connect(mapStateToProps, null)(ProfilePage));