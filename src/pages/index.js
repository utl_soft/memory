export { default as DashboardPage } from './DashboardPage';
export { default as ProfilePage } from './ProfilePage';
export { default as LeaderBoardPage } from './LeaderBoardPage';
export { default as CoursesPage } from './CoursesPage';
export { default as LearnPage } from './LearnPage';