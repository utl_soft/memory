import React, { Component } from 'react';

import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';

import styled from 'styled-components';

import CoursesComponent from 'components/CoursesComponent/CoursesComponent';

const RowCourses = styled('div')`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding-right: 25px;
`;

const mapStateToProps = state => ({
  courses: state.courses.courses
});

class CoursesPage extends Component {
  render() {
    const { courses } = this.props;
    return (
      <RowCourses gutter={16}>
        {
          courses && <CoursesComponent courses={courses} />
        }
      </RowCourses>
    )
  }
}

export default withRouter(connect(mapStateToProps, null)(CoursesPage));