import React, { Component } from 'react';

import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';

import { Spin } from 'antd';

import { getCurrentCourse } from 'actions/courses';

import TreeComponent from 'components/CoursesComponent/TreeComponent';

import store from 'store';

const mapStateToProps = state => ({
  courses: state.courses.currentCourse,
  load: state.courses.load
});

class LearnPage extends Component {
  componentDidMount() {
    store.dispatch(getCurrentCourse(this.props.match.params.categoryId));
  }

  sortingCategories = courses => {
    let categories = {
      category: [],
      ids: [],
    };
    if (courses && courses.subcategories) {
      courses.subcategories.map(course => {
        categories.category.push(course);

        if (course.next_categories){
          course.next_categories.map((next_category) => {
            categories.ids = [
              ...categories.ids,
              {
                from: course.category_id,
                to: next_category.category_id
              }
            ];
            return categories
          });
        }
        return course;
      });
    }
    return categories;
  };

  render() {
    const { courses, load } = this.props;

    return (
      <div>
        {!load ? <TreeComponent categories={this.sortingCategories(courses)} /> :
          <div style={{ textAlign: 'center' }}>
            <Spin size="large" />
          </div>}
      </div>
    )
  }
}

export default withRouter(connect(mapStateToProps, null)(LearnPage));