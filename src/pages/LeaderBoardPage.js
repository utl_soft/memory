import React, { Component } from 'react';

import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';

import LeaderBoardComponent from 'components/LeaderBoardComponent/LeaderBoardComponent';

class LeaderBoardPage extends Component {
  render() {
    return (
      <div>
        <LeaderBoardComponent />
      </div>
    )
  }
}

export default withRouter(connect()(LeaderBoardPage));