FROM node:8-alpine as buildContainer

COPY . /app
WORKDIR /app
RUN yarn install
RUN npm run build

FROM node:8-alpine

RUN yarn global add serve

WORKDIR /app

# Get all the code needed to run the app
COPY --from=buildContainer /app/build /app

# Expose the port the app runs in
EXPOSE 5000

# Serve the app
CMD ["serve", "-s"]

